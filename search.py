# search.py
# ---------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
#
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


"""
In search.py, you will implement generic search algorithms which are called by
Pacman agents (in searchAgents.py).
"""

import util
from pprint import pprint

class SearchProblem:
    """
    This class outlines the structure of a search problem, but doesn't implement
    any of the methods (in object-oriented terminology: an abstract class).

    You do not need to change anything in this class, ever.
    """

    def getStartState(self):
        """
        Returns the start state for the search problem.
        """
        util.raiseNotDefined()

    def isGoalState(self, state):
        """
          state: Search state

        Returns True if and only if the state is a valid goal state.
        """
        util.raiseNotDefined()

    def getSuccessors(self, state):
        """
          state: Search state

        For a given state, this should return a list of triples, (successor,
        action, stepCost), where 'successor' is a successor to the current
        state, 'action' is the action required to get there, and 'stepCost' is
        the incremental cost of expanding to that successor.
        """
        util.raiseNotDefined()

    def getCostOfActions(self, actions):
        """
         actions: A list of actions to take

        This method returns the total cost of a particular sequence of actions.
        The sequence must be composed of legal moves.
        """
        util.raiseNotDefined()


def tinyMazeSearch(problem):
    """
    Returns a sequence of moves that solves tinyMaze.  For any other maze, the
    sequence of moves will be incorrect, so only use this for tinyMaze.
    """

    from game import Directions
    s = Directions.SOUTH
    w = Directions.WEST
    return  [s, s, w, s, w, w, s, w]

def depthFirstSearch(problem):
    """
    Search the deepest nodes in the search tree first.

    Your search algorithm needs to return a list of actions that reaches the
    goal. Make sure to implement a graph search algorithm.
    print "Start:", problem.getStartState()
    print "Is the start a goal?", problem.isGoalState(problem.getStartState())
    print "Start's successors:", problem.getSuccessors(problem.getStartState())
    """
#    print "start is : " ,   problem.getSuccessors((33,16))


    snode = problem.getStartState()
    from game import Directions

    openlist=util.Stack()
    vistited=[]
    actions=[]
    parentmap={}
    openlist.push((problem.getStartState(),0,0))

    while not openlist.isEmpty():

        currentNode = openlist.pop()

        if problem.isGoalState(currentNode[0]):


            while(not currentNode[0]  == snode):

                action =currentNode[1]
                currentNode = parentmap [currentNode]
                actions.append(action)

            actions = list(reversed(actions))

            return actions
            break

        if currentNode not in vistited:

            vistited.append(currentNode)

            for child in problem.getSuccessors(currentNode[0]):
                    if child not in vistited:
                        openlist.push(child)
                        parentmap[child] = currentNode






def breadthFirstSearch(problem):
    """Search the shallowest nodes in the search tree first."""

    Snode = problem.getStartState()
    from game import Directions

    openlist=util.Queue()
    vistited=[]
    actions=[]
    parentmap={}
    openlist.push((problem.getStartState(),0,0))

    while not openlist.isEmpty():

        currentNode   =  openlist.pop()

        if problem.isGoalState(currentNode[0]):


            while(not currentNode[0]  == Snode):

                action =currentNode[1]
                currentNode = parentmap [currentNode]
                actions.append(action)

            actions = list(reversed(actions))

            return actions
            break

        if currentNode not in vistited:

            vistited.append(currentNode)

            for child in problem.getSuccessors(currentNode[0]):
                    if child not in vistited:
                        openlist.push(child)
                        parentmap[child] = currentNode



def uniformCostSearch(problem):
    """Search the node of least total cost first."""

    Snode = problem.getStartState()

    openlist=util.PriorityQueue()
    vistited=[]
    actions=[]
    parentmap={}
    openlist.push((problem.getStartState(),0,0),1)

    while not openlist.isEmpty():

        currentNode   =  openlist.pop()

        if problem.isGoalState(currentNode[0]):


            while(not currentNode[0]  == Snode):

                action =currentNode[1]
                currentNode = parentmap [currentNode]
                actions.append(action)

            actions = list(reversed(actions))
            print actions
            print "****************"
            return actions
            break

        if currentNode not in vistited:

            vistited.append(currentNode)

            for child in problem.getSuccessors(currentNode[0]):
                    if child not in vistited:

                        openlist.push(child,child[2])

                        parentmap[child] = currentNode


def nullHeuristic(state, problem):
    """
    A heuristic function estimates the cost from the current state to the nearest
    goal in the provided SearchProblem.  This heuristic is trivial.
    """
    xy1 = state
    xy2 = problem.goal
    print xy2
    return abs(xy1[0] - xy2[0]) + abs(xy1[1] - xy2[1])

def euclideanHeuristic(position, problem):
    "The Euclidean distance heuristic for a PositionSearchProblem"
    xy1 = position
    xy2 = problem.goal
    return ( (xy1[0] - xy2[0]) ** 2 + (xy1[1] - xy2[1]) ** 2 ) ** 0.5



def aStarSearch(problem, heuristic=euclideanHeuristic):
    """Search the node that has the lowest combined cost and heuristic first."""
    "*** YOUR CODE HERE ***"
    print heuristic((5,6) ,problem)
    Snode = problem.getStartState()

    openlist=util.PriorityQueue()
    vistited=[]
    actions=[]
    parentmap={}
    openlist.push((problem.getStartState(),0,0),1)

    while not openlist.isEmpty():

        currentNode   =  openlist.pop()

        if problem.isGoalState(currentNode[0]):


            while(not currentNode[0]  == Snode):

                action =currentNode[1]
                currentNode = parentmap [currentNode]
                actions.append(action)

            actions = list(reversed(actions))

            return actions
            break

        if currentNode not in vistited:

            vistited.append(currentNode)

            for child in problem.getSuccessors(currentNode[0]):
                    if child not in vistited:

                        openlist.push(child,heuristic(child[0] ,problem)+child[2])

                        parentmap[child] = currentNode
#    util.raiseNotDefined()


# Abbreviations
bfs = breadthFirstSearch
dfs = depthFirstSearch
astar = aStarSearch
ucs = uniformCostSearch
